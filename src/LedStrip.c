#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#include "stm32f0xx.h"

#include "Utils.h"

#include "LedStrip.h"
#include "FlashDataTable.h"

static void EnableDMA(bool enable)
{
  if (enable)
    REG_SET_BIT(DMA1_Channel5->CCR, DMA_CCR_EN);
  else
    REG_CLEAR_BIT(DMA1_Channel5->CCR, DMA_CCR_EN);
}

static void UpdateDMATransferCount(uint16_t count)
{
  DMA1_Channel5->CNDTR = count;
}

void WriteLedColor(const void * _instance, uint32_t ledNumber, uint32_t rgbColor)
{
  REINTERPRET(instance, _instance, LedStrip_t *);

  // Bits in GRB order
  uint32_t grb = (rgbColor & 0xFF00) << 8 | (rgbColor & 0xFF0000) >> 8 | (rgbColor & 0xFF);
  uint8_t tmp[NumberOfBitsInOneColor];
  for (uint8_t i = 0; i < NumberOfBitsInOneColor; i++)
  {
    tmp[i] = (grb & (1 << ((NumberOfBitsInOneColor - 1) - i))) ? Bit1PwmValue :
                                                               Bit0PwmValue;
  }
  uint32_t actualLedNumber = ledNumber > instance->_private.numberOfLeds ? instance->_private.numberOfLeds : ledNumber;

  if (!ProgramData(&FLASHDATATABLE->ledDataStream.data[actualLedNumber * NumberOfBitsInOneColor], tmp, sizeof(*tmp) * NumberOfBitsInOneColor))
  {
    while(1);
  }

  EnableDMA(false);
  UpdateDMATransferCount(instance->_private.numberOfLeds * NumberOfBitsInOneColor + NumberOfResetBits);
  EnableDMA(true);
}

LedStrip_Api_t api =
{ .WriteLedColor = WriteLedColor };

static void SetupGpio()
{
  REG_SET_BIT(RCC->AHBENR, RCC_AHBENR_GPIOAEN);
  REG_SET_BIT(GPIOA->MODER, MODERx_AF(8));
  REG_SET_BIT(GPIOA->AFR[1], AFRx_SELECT(2, 8));
}

static void SetupDma(uint8_t * data)
{
  REG_SET_BIT(RCC->AHBENR, RCC_AHBENR_DMA1EN);
  REG_CLEAR_BIT(DMA1_Channel5->CCR, DMA_CCR_EN);
  DMA1_Channel5->CNDTR = 0;
  DMA1_Channel5->CMAR = (uint32_t) data;
  DMA1_Channel5->CPAR = (uint32_t) &TIM1->CCR1;
  DMA1_Channel5->CCR = DMA_CCR_PSIZE_0 | DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_EN;
}

static void SetupPwm()
{
  REG_SET_BIT(RCC->APB2ENR, RCC_APB2ENR_TIM1EN);
  TIM1->PSC = 1 - 1;
  TIM1->ARR = 80 - 1;
  REG_SET_BIT(TIM1->CCMR1, OCxM1_PWM1(1) | TIM_CCMR1_OC1PE);
  TIM1->CCR1 = 0;
  REG_SET_BIT(TIM1->BDTR, TIM_BDTR_MOE);
  REG_SET_BIT(TIM1->DIER, TIM_DIER_UDE);
  REG_SET_BIT(TIM1->CCER, TIM_CCER_CC1E);
  REG_SET_BIT(TIM1->CR1, TIM_CR1_CEN);
}

LedStrip_t * LedStrip_Init(uint16_t numberOfLeds)
{
  static LedStrip_t instance;
  instance._private.numberOfLeds = numberOfLeds > MaxNumberOfLeds ? MaxNumberOfLeds : numberOfLeds;
  instance.api = &api;

  SetupGpio();

  uint32_t numberOfColorBits = instance._private.numberOfLeds * NumberOfBitsInOneColor;
  LedDataStream_t tmp;
  for (uint32_t i = 0; i < numberOfColorBits; i++)
  {
    tmp.data[i] = Bit0PwmValue;
  }
  for (uint8_t i = 0; i < NumberOfResetBits; i++)
  {
    tmp.data[numberOfColorBits + i] = 0;
  }

  ProgramData(&FLASHDATATABLE->ledDataStream, &tmp, sizeof(tmp));

  SetupDma(FLASHDATATABLE->ledDataStream.data);
  SetupPwm();

  EnableDMA(false);
  UpdateDMATransferCount(numberOfColorBits + NumberOfResetBits);
  EnableDMA(true);

  return &instance;
}
