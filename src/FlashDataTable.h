#ifndef FLASHDATATABLE_H
#define FLASHDATATABLE_H

#include <stdint.h>
#include "DataTable.h"

#define FLASH_PAGE_SIZE         ((uint32_t)0x00000400)   /* FLASH Page Size */
#define FLASH_USER_START_ADDR   ((uint32_t)0x08001000)   /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     ((uint32_t)0x0800FC00)   /* End @ of user Flash area */

#define FLASHDATATABLE ((DataTable_t *) FLASH_USER_START_ADDR)

typedef struct
{
} FlashDataTable_Api_t;

typedef struct
{
  FlashDataTable_Api_t *api;

  struct
  {
  } _private;
} FlashDataTable_t;

FlashDataTable_t * FlashDataTable_Init();
bool ProgramData(const void * flashLocation, const void * data, int sizeInBytes);

#endif
