#ifndef DATATABLE_H
#define DATATABLE_H

#include "LedStrip.h"

typedef struct
{
  uint16_t version;
  LedDataStream_t ledDataStream;
} DataTable_t;

#endif
