#ifndef LEDSTRIP_H
#define LEDSTRIP_H

#include <stdint.h>

enum
{
  MaxNumberOfLeds = 64,
  NumberOfBitsInOneColor = 24,
  NumberOfResetBits = 204,
  Bit0PwmValue = 13,
  Bit1PwmValue = 67,
};

typedef struct
{
  uint8_t data[MaxNumberOfLeds * NumberOfBitsInOneColor + NumberOfResetBits];
} LedDataStream_t;

typedef struct
{
  void (*WriteLedColor)(const void * instance, uint32_t ledNumber, uint32_t rgbColor);
} LedStrip_Api_t;

typedef struct
{
  LedStrip_Api_t *api;

  struct
  {
    uint16_t numberOfLeds;
  } _private;
} LedStrip_t;

LedStrip_t * LedStrip_Init(uint16_t numberOfLeds);

#define LedStrip_WriteLedColor(instance, ledNumber, rgbColor) (instance)->api->WriteLedColor((instance), (ledNumber), (rgbColor))

#endif
