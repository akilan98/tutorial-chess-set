#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "stm32f0xx.h"

#include "FlashDataTable.h"
//#include "Display.h"
#include "LedStrip.h"
//#include "PiecePositionSensorMatrix.h"
//#include "Button.h"
//#include "ButtonMenu.h"
#include "Utils.h"

void nano_wait(int);

void test_led() {
	LedStrip_t * ledStrip = LedStrip_Init(64);
	uint32_t colors[3] = { 0xFF0000, 0x00FF00, 0x0000FF };

	for (uint8_t i = 0; i < 64; i++) {
		LedStrip_WriteLedColor(ledStrip, i, colors[i % 3]);
	}
}

void test_flash() {
	FlashDataTable_Init();
	uint16_t test = 0xABCD;
	if (ProgramData(&FLASHDATATABLE->version, &test, sizeof(test))) {
		REG_SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS_8);
	}

	if (FLASHDATATABLE->version == test) {
		REG_SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS_9);
	}

	LedDataStream_t tmp;
	for (uint16_t i = 0; i < 1200; i++) {
		tmp.data[i] = test;
	}
	if (ProgramData(&FLASHDATATABLE->ledDataStream, &tmp,
			(uint32_t) &tmp.data[1200] - (uint32_t) &tmp)) {
		for (uint16_t i = 0; i < 1200; i++) {
			if (FLASHDATATABLE->ledDataStream.data[i] != (uint8_t) test) {
				while (1)
					;
			}
		}
		while (1) {
			REG_TOGGLE_BIT(GPIOC->ODR, GPIO_ODR_9|GPIO_ODR_8);
			nano_wait(1000000000 / 2);
		}
	}
}

int main() {
//  Display_t * display = Display_Init();
//  PiecePositionSensorMatrix_t * positionSensorMatrix = PiecePositionSensorMatrix_Init();
//  Button * turnButton = Button_Init();
//  ButtonMenu * optionButtons = Button_Init();

	REG_SET_BIT(RCC->AHBENR, RCC_AHBENR_GPIOCEN);
	REG_SET_BIT(GPIOC->MODER, GPIO_MODER_MODER9_0|GPIO_MODER_MODER8_0);
	REG_SET_BIT(GPIOC->BRR, GPIO_BRR_BR_9|GPIO_BRR_BR_8);

	FlashDataTable_t * flashDataTable = FlashDataTable_Init();
	test_led();
//  test_flash();

	while (1)
		;
	return 0;
}
