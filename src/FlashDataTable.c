#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#include "FlashDataTable.h"
#include "Utils.h"

#include "stm32f0xx.h"

static void WaitUntilOperationIsComplete()
{
  while (FLASH->SR & FLASH_SR_BSY)
    ;
}

static bool ErrorsInOperation()
{
  if (FLASH->SR & (FLASH_SR_PGERR | FLASH_SR_WRPERR))
  {
    REG_SET_BIT(FLASH->SR, FLASH_SR_WRPERR|FLASH_SR_PGERR);
    return true;
  }
  return false;
}

static void UnlockFlash()
{
  if (FLASH->CR & FLASH_CR_LOCK)
  {
    FLASH->KEYR = FLASH_FKEY1;
    FLASH->KEYR = FLASH_FKEY2;
  }
}

static void LockFlash()
{
  REG_SET_BIT(FLASH->CR, FLASH_CR_LOCK);
}

static void ErasePage(uint32_t address)
{
  UnlockFlash();
  REG_SET_BIT(FLASH->CR, FLASH_CR_PER);
  FLASH->AR = address;
  REG_SET_BIT(FLASH->CR, FLASH_CR_STRT);
  WaitUntilOperationIsComplete();
  if (FLASH->SR & FLASH_SR_EOP)
  {
    REG_SET_BIT(FLASH->SR, FLASH_SR_EOP);
  }
  REG_CLEAR_BIT(FLASH->CR, FLASH_CR_PER);
  LockFlash();
}

static void EraseAllFlash()
{
  UnlockFlash();
  REG_SET_BIT(FLASH->CR, FLASH_CR_PER);
  FLASH->AR = FLASH_USER_START_ADDR;
  uint32_t addr = FLASH_USER_START_ADDR;
  while (addr < FLASH_USER_END_ADDR)
  {
    FLASH->AR = addr;
    REG_SET_BIT(FLASH->CR, FLASH_CR_STRT);
    WaitUntilOperationIsComplete();
    if (FLASH->SR & FLASH_SR_EOP)
    {
      REG_SET_BIT(FLASH->SR, FLASH_SR_EOP);
    }
    addr += FLASH_PAGE_SIZE;
  }
  REG_CLEAR_BIT(FLASH->CR, FLASH_CR_PER);
  LockFlash();
}

static bool ProgramPage(const void * address, const void * data)
{
  REINTERPRET(addressLocation, address, uint32_t);
  addressLocation = addressLocation & 0xFFFFFC00;
  REINTERPRET(dataLocation, data, uint32_t);
  ErasePage(addressLocation);
  UnlockFlash();
  REG_SET_BIT(FLASH->CR, FLASH_CR_PG);
  for (uint16_t i = 0; i < FLASH_PAGE_SIZE / 2; i++)
  {
    *(__IO uint16_t *) addressLocation = *(uint16_t *) dataLocation;
    WaitUntilOperationIsComplete();
    if (ErrorsInOperation())
    {
      ErasePage(addressLocation);
      return false;
    }
    if (FLASH->SR & FLASH_SR_EOP)
    {
      REG_SET_BIT(FLASH->SR, FLASH_SR_EOP);
    }
    addressLocation += 2;
    dataLocation += 2;
  }
  REG_CLEAR_BIT(FLASH->CR, FLASH_CR_PG);
  LockFlash();
  return true;
}

bool ProgramData(const void * flashLocation, const void * data, int sizeInBytes)
{
  REINTERPRET(flashAddress, flashLocation, uint32_t);
  REINTERPRET(dataAddress, data, uint32_t);
  if (sizeInBytes == 0) return true;
  if (sizeInBytes > (FLASH_USER_END_ADDR - flashAddress)) return false;
  uint32_t pageStartAddress = flashAddress & 0xFFFFFC00;;
  uint8_t page[1024];
  while (sizeInBytes > 0)
  {
    memcpy(page, pageStartAddress, sizeof(uint8_t) * FLASH_PAGE_SIZE);
    uint16_t dataSize = sizeInBytes > FLASH_PAGE_SIZE - (flashAddress - pageStartAddress) ? FLASH_PAGE_SIZE - (flashAddress - pageStartAddress) : sizeInBytes;
    memcpy(page + (flashAddress - pageStartAddress), dataAddress, dataSize);
    if (!ProgramPage(pageStartAddress, page))
    {
      return false;
    }
    pageStartAddress += FLASH_PAGE_SIZE;
    flashAddress = pageStartAddress;
    sizeInBytes -= dataSize;
    dataAddress += dataSize;
  }
  return true;
}

FlashDataTable_t * FlashDataTable_Init()
{
  static FlashDataTable_t instance;
  WaitUntilOperationIsComplete();
  EraseAllFlash();

  WaitUntilOperationIsComplete();

  return &instance;
}
