#ifndef UTILS_H
#define UTILS_H

#define NULL ((void *)0)
#define REINTERPRET(to, from, type) type to = ((type)(from))

#define REG_SET_BIT(reg, word) ((reg) |= (word))
#define REG_CLEAR_BIT(reg, word) ((reg) &= ~(word))
#define REG_TOGGLE_BIT(reg, word) ((reg) ^= (word))

#define BIT_SET(word, bit) REG_SET_BIT(word, 0x1 << (bit))
#define BIT_CLEAR(word, bit) REG_CLEAR_BIT(word, 0x1 << (bit))
#define BIT_TOGGLE(word, bit) REG_TOGGLE_BIT(word, 0x1 << (bit))
#define BIT_SELECT(word, bit) ((word) & (1 << bit))

#define IGNORE(x) ((void)x)

// GPIOx
#define MODERx_OUTPUT(pin) (GPIO_MODER_MODER##pin##_0)
#define MODERx_AF(pin) (GPIO_MODER_MODER##pin##_1)
#define AFRx_SELECT(af, pin) ((af) << (((pin) % 8) * 4))
#define PUPDR_PULLUP(pin) (GPIO_PUPDR_PUPDR##pin##_0)
#define PUPDR_PULLDOWN(pin) (GPIO_PUPDR_PUPDR##pin##_1)

// TIMx
#define OCxM1_TOGGLE(pin) (TIM_CCMR1_OC##pin##M_1|TIM_CCMR1_OC##pin##M_0)
#define OCxM2_TOGGLE(pin) (TIM_CCMR2_OC##pin##M_1|TIM_CCMR2_OC##pin##M_0)

#define OCxM1_PWM1(pin) (TIM_CCMR1_OC##pin##M_2|TIM_CCMR1_OC##pin##M_1)
#define OCxM2_PWM1(pin) (TIM_CCMR2_OC##pin##M_2|TIM_CCMR2_OC##pin##M_1)

#define OCxM1_PWM2(pin) (TIM_CCMR1_OC##pin##M_2|TIM_CCMR1_OC##pin##M_1|TIM_CCMR1_OC##pin##M_0)
#define OCxM2_PWM2(pin) (TIM_CCMR2_OC##pin##M_2|TIM_CCMR2_OC##pin##M_1|TIM_CCMR2_OC##pin##M_0)

#endif
